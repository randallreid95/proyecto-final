console.log('Cargando Cards...')
const dataCards = [
    {
        "title": "Hexacampeonato",
        "url_image": "https://static3.teletica.com/Files/Sizes/2019/3/26/254354735_1140x520.jpg",
        "desc": "El Deportivo Saprissa impuso una marca nacional y regional al consagrarse campeón de seis campeonatos de forma consecutiva en 1972, 1973, 1974, 1975, 1976 y 1977 y hasta la fecha es un record que ningun otro club a podido igualar.",
        "cta": "Mostrar más...",
        "link": "https://cicadex.com/homepage/saprissa-hexacampeon-2019/"
    },
    {
        "title": "Mundial de clubes",
        "url_image": "https://buzonderodrigo.com/wp-content/uploads/2018/04/Optimized-Arabes1-1024x647.jpg",
        "desc": "Saprissa se consagró campeón de la copa concacaf 2004 que le permitió disputar el mundial de clubes en 2005 en Japón y, tras una gran campaña se consagró con un meritorio tercer lugar",
        "cta": "Mostrar más...",
        "link": "https://buzonderodrigo.com/historico-tercer-lugar-mundial-del-saprissa-videos/"
    },
    {
        "title": "La vuelta al mundo",
        "url_image": "https://historiasdelfutboltico.files.wordpress.com/2016/04/delegacic3b3n-saprissa-gira-mundial.jpg",
        "desc": "El Deportivo Saprissa en 1959 realizó una proeza catalogada hoy en dia por lo que significó. El club realizó una gira extensiva de 71 días por 25 países distintos de 4 continentes diferentes y que califica como la primera vuelta al mundo hecha por algún equipo de fútbol en el mundo.",
        "cta": "Mostrar más...",
        "link": "https://historiasdelfutboltico.wordpress.com/2016/04/04/saprissa-primer-equipo-en-darle-una-vuelta-al-mundo/"
    },
   ];

    (function () {
        let CARD = {
            init: function (){
                console.log('El modulo de carga funciona correctamente');
                let _self = this;
                //llamamos a las funciones
                this.insertData(_self);
            },

            eventHandler: function (_self){
                let arrayRefs = document.querySelectorAll('.accordion-title');
                for(let x = 0; x < arrayRefs.length; x++){
                    arrayRefs[x].addEventListener('click', function(event){
                        console.log('Evento: ', event);
                        _self.showTab(event.target);
                    });
                }
            },

            insertData: function (_self){
                dataCards.map(function (item, index){
                    document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
                });                
            },

            tplCardItem: function (item, index){
                return(`<div class='card-item' id="card-number-${index}">
                <img src="${item.url_image}"/>
                <div class="card-info">
                <p class='card-title'>${item.title}</p>
                <p class='card-desc'>${item.desc}</p>
                <a class='card-cta' target ='blank' href="${item.link}">${item.cta}</a>
            </div>
            </div>`)
        },
    }

    CARD.init();
})();
