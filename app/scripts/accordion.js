console.log('Cargando Accordion...');
const dataAccordion = [
    {
        "title": "Sede",
        "desc": "Tibás, San José"
    },
    {
        "title": "Apodos",
        "desc": "El Monstruo​, Los Morados​, El Sapri,​ La S,​ Rey de Copas​"
    },
];

(function () {
    let ACCORDION = {
        init: function (){
            let _self = this;
            //Llamamos funciones
            this.insertData(_self);
            this.eventHandler(_self);
        },

        eventHandler: function (_self){
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++){
                arrayRefs[x].addEventListener('click', function(event){
                 console.log('event', event);
                 _self.showTab(event.target);
             });
            }
        
    },

    showTab: function(refItem){
        let activeTab = document.querySelector('.tab-active');

        if(activeTab){
            activeTab.classList.remove('tab-active');
        }

        console.log('show tab', refItem);
        refItem.parentElement.classList.toggle('tab-active');
    },

    insertData: function (_self) {
        dataAccordion.map(function (item, index){
            document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));

            });
    },

    tplAccordionItem: function(item){
        return(`<div class='accordion-item'>
        <div class='accordion-title'><p>${item.title}</p></div>
        <div class='accordion-desc'><p>${item.desc}</p></div>
        </div>`)
    },

}
    ACCORDION.init();
})();